package com.jefmed.dgawaula14e1.repositories;

import com.jefmed.dgawaula14e1.models.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlunoRepository extends JpaRepository<Aluno, Integer> {
}
