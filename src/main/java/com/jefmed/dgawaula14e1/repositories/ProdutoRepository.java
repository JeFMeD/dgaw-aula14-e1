package com.jefmed.dgawaula14e1.repositories;

import com.jefmed.dgawaula14e1.models.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Integer> {
}
