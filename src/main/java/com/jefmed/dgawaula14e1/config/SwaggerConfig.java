package com.jefmed.dgawaula14e1.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    
    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("products")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.jefmed.dgawaula14e1"))
                .paths(regex("/product.*"))
                .build()
                .apiInfo(metaInfo());
    }

    @Bean
    public Docket AlunoApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("alunos")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.jefmed.dgawaula14e1"))
                .paths(regex("/aluno.*"))
                .build()
                .apiInfo(metaInfo());
    }
    
    private ApiInfo metaInfo() {

        return new ApiInfoBuilder()
                .title ("API REST CADASTRO")
                .description ("API REST para manipulação de dados")
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                .termsOfServiceUrl("/service.html")
                .version("1.0.0")
                .contact(new Contact(
                        "Jeferson Medeiros",
                        "https://github.com/jefmed",
                        "jeferson.medeiros@gmail.com"))
                .build();
    }
}
