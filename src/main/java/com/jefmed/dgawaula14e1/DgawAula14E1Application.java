package com.jefmed.dgawaula14e1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DgawAula14E1Application {

    public static void main(String[] args) {
        SpringApplication.run(DgawAula14E1Application.class, args);
    }

}
