package com.jefmed.dgawaula14e1.controllers;

import com.jefmed.dgawaula14e1.models.Produto;
import com.jefmed.dgawaula14e1.services.ProdutoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
@CrossOrigin(origins = "*")
public class ProdutoController {
    
    @Autowired
    private ProdutoService produtoService;
    
    @ApiOperation(value = "Retorna todos os produtos")
    @GetMapping
    public ResponseEntity<Object> getAllProducts() {
        return ResponseEntity.ok(produtoService.getAllProducts());
    }
    
    @ApiOperation(value = "Retorna um produto de acordo com o ID informado")
    @GetMapping("/{id}")
    public ResponseEntity<Produto> getProductById(@PathVariable int id) {
        return ResponseEntity.ok(produtoService.getPorductById(id));
    }
    
    @ApiOperation(value = "Cria um novo produto")
    @PostMapping("/create")
    public ResponseEntity<Produto> createProduct(@RequestBody Produto produto) {
        Produto novoProduto = produtoService.saveProduct(produto);
        return ResponseEntity.status(HttpStatus.CREATED).body(novoProduto);
    }
    
    @ApiOperation(value = "Atualiza um produto ja existente")
    @PutMapping
    public ResponseEntity<Produto> updateProduct(@RequestBody Produto produto) {
        return ResponseEntity.ok(produtoService.updateProduct(produto));
    }
    
    @ApiOperation(value = "Remove um produto ja existente")
    @DeleteMapping("/{id}")
    public ResponseEntity<Produto> deleteProduct(@PathVariable int id) {
        return ResponseEntity.ok(produtoService.deleteProductById(id));
    }
    
    
    
}
