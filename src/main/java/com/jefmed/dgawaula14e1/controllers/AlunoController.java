package com.jefmed.dgawaula14e1.controllers;


import com.jefmed.dgawaula14e1.models.Aluno;
import com.jefmed.dgawaula14e1.services.AlunoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/aluno")
@CrossOrigin(origins = "*")
public class AlunoController {

    @Autowired
    private AlunoService alunoService;

    @ApiOperation(value = "Retorna todos os alunos")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucesso ao listar alunos"),
            @ApiResponse(code = 405, message = "Erro na validação"),
    })
    @GetMapping
    public ResponseEntity<Object> getAllAlunos() {
        try {
            return ResponseEntity.ok(alunoService.getAllAlunos());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        }
    }

    @ApiOperation(value = "Retorna um aluno de acordo com o ID informado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucesso ao buscar um aluno"),
            @ApiResponse(code = 405, message = "Erro na validação"),
    })
    @GetMapping("/{id}")
    public ResponseEntity<Aluno> getAlunoById(@PathVariable int id) {
        try {
            return ResponseEntity.ok(alunoService.getAlunoById(id));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        }
    }

    @ApiOperation(value = "Cria um novo aluno")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Sucesso na criação de aluno"),
            @ApiResponse(code = 405, message = "Erro na validação"),
    })
    @PostMapping("/create")
    public ResponseEntity<Aluno> createAluno(@RequestBody Aluno aluno) {
        try {
            Aluno novoAluno = alunoService.saveProduct(aluno);
            return ResponseEntity.status(HttpStatus.CREATED).body(novoAluno);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        }

    }

    @ApiOperation(value = "Atualiza um aluno ja existente")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucesso ao atualizar o aluno"),
            @ApiResponse(code = 405, message = "Erro na validação"),
    })
    @PutMapping
    public ResponseEntity<Aluno> updateAluno(@RequestBody Aluno aluno) {
        try {
            return ResponseEntity.ok(alunoService.updateAluno(aluno));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        }
    }

    @ApiOperation(value = "Remove um produto ja existente")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucesso ao remover aluno"),
            @ApiResponse(code = 405, message = "Erro na remoção"),
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Aluno> deleteAluno(@PathVariable int id) {
        try {
            return ResponseEntity.ok(alunoService.deleteAlunoById(id));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        }
    }
}
