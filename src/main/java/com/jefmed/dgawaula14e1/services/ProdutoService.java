package com.jefmed.dgawaula14e1.services;

import com.jefmed.dgawaula14e1.models.Produto;
import com.jefmed.dgawaula14e1.repositories.ProdutoRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProdutoService {
    private final ProdutoRepository produtoRepository;

    public ProdutoService(ProdutoRepository produtoRepository) {
        this.produtoRepository = produtoRepository;
    }

    public Produto saveProduct(Produto produto){
        return this.produtoRepository.save(produto);
    }

    public List<Produto> getAllProducts() {
        return new ArrayList<Produto>(this.produtoRepository.findAll());
    }

    public Produto getPorductById(int id) {
        return this.produtoRepository.getOne(id);
    }
    
    public Produto updateProduct(Produto produto) {
        Produto produtoASerEditado = getPorductById(produto.getId());
        produtoASerEditado.setName(produto.getName());
        produtoASerEditado.setPrice(produto.getPrice());
        saveProduct(produtoASerEditado);
        return produtoASerEditado;
    }
    
    public Produto deleteProductById(int id) {
        Produto produto = getPorductById(id);
        this.produtoRepository.deleteById(id);
        return produto;
    }
}
