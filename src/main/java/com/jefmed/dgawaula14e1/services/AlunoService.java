package com.jefmed.dgawaula14e1.services;


import com.jefmed.dgawaula14e1.models.Aluno;
import com.jefmed.dgawaula14e1.repositories.AlunoRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AlunoService {
    private final AlunoRepository alunoRepository;


    public AlunoService(AlunoRepository alunoRepository) {
        this.alunoRepository = alunoRepository;
    }

    public Aluno saveProduct(Aluno aluno){
        return this.alunoRepository.save(aluno);
    }

    public List<Aluno> getAllAlunos() {
        return new ArrayList<Aluno>(this.alunoRepository.findAll());
    }

    public Aluno getAlunoById(int id) {
        return this.alunoRepository.getOne(id);
    }

    public Aluno updateAluno(Aluno aluno) {
        Aluno alunoASerEditado = getAlunoById(aluno.getId());
        alunoASerEditado.setName(aluno.getName());
        alunoASerEditado.setEmail(aluno.getEmail());
        alunoASerEditado.setRegistration(aluno.getRegistration());
        saveProduct(alunoASerEditado);
        return alunoASerEditado;
    }

    public Aluno deleteAlunoById(int id) {
        Aluno aluno = getAlunoById(id);
        this.alunoRepository.deleteById(id);
        return aluno;
    }
}
