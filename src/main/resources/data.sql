DROP TABLE IF EXISTS produto;
DROP TABLE IF EXISTS aluno;

CREATE TABLE produto (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR_IGNORECASE NOT NULL,
  price DOUBLE NOT NULL
);

CREATE TABLE aluno (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR_IGNORECASE NOT NULL,
  email VARCHAR_IGNORECASE NOT NULL,
  registration INT NOT NULL
);

INSERT INTO produto (name, price) VALUES
  ('Jeferson', 5.5),
  ('Nina', 10.49);

INSERT INTO aluno (name, email, registration) VALUES
  ('Jeferson', 'jeferson@email.com', 1234567890),
  ('Nina', 'nina@email.com', 1987654321);